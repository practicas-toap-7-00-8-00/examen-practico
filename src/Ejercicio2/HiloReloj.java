package Ejercicio2;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author Windows 10
 */
public class HiloReloj extends Thread{
    
    private final JLabel jLabel1;

    public HiloReloj(JLabel jLabelReloj){ 
        this.jLabel1 = jLabelReloj;
    }
    
    @Override
    public void run() {
        while(true){
            Date fecha = new Date();
            SimpleDateFormat sdt = new SimpleDateFormat("hh:mm:ss a");
            jLabel1.setText(sdt.format(fecha));
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(HiloReloj.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
